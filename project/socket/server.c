/* --- server.c --- */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

char message_from_server[] = "selam client, ben server";

int main(int argc, char *argv[])
{
	int listenfd = 0, connfd = 0;
	
	struct sockaddr_in serv_addr;

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(5000);

	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	listen(listenfd, 10);

	while(1)
	{
		connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
		write(connfd, message_from_server , strlen(message_from_server));
		close(connfd);
		sleep(1);
	}
}

