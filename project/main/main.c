#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "timer.h"
#include "netfilter.h"
#include "fork_and_execute.h"

void exit_sys(const char *msg);
void exit_fail(const char *msg);
void start_netfilter_ioctl();
void stop_netfilter_ioctl();

int fd;

int main(int argc, char *argv[])
{
    int result;
    int a_flag = 0, b_flag = 0, c_flag = 0, d_flag = 0;
    char *a_arg;
    int i;

    if (argc < 2)
    {
        printf("usage ---->  ./main -<a:bcd> \"program args\" \n");
        exit(EXIT_FAILURE);
    }

    while ((result = getopt(argc, argv, "a:bcd")) != -1)
    {
        switch (result)
        {
        case 'a':
            a_flag = 1;
            a_arg = optarg;
            break;
        case 'b':
            b_flag = 1;
            break;
        case 'c':
            c_flag = 1;
            break;
        case 'd':
            c_flag = 1;
            break;
        case '?':
            if (optopt == 'a')
                fprintf(stderr, "a switch without argument!..\n");
            else
                fprintf(stderr, "invalid switch: -%c \n \n usage ---->  ./main -<a:bc> \"program args\"  \n", optopt);
            exit(EXIT_FAILURE);
        }
    }

    if (a_flag)
    {
        int j = 0;
        char arg[1024];
        printf("a switch specified --> %s will be executed...\n", a_arg);
        fork_execute(a_arg);
    }

    if (b_flag)
    {
        printf("b switch specified, timer started...\n");
        i = 0;
        start_timer();
        while (1)
        {
            sleep(2);
            i++;
            if (i == 5)
            {
                delete_timer();
                break;
            }
        }
    }
    if (c_flag)
    {

        struct sigaction act;

        act.sa_handler = start_netfilter_ioctl;
        sigemptyset(&act.sa_mask);
        act.sa_flags = 0;

        if (sigaction(SIGUSR1, &act, NULL) == -1)
            exit_sys("sigaction");

        struct sigaction act2;

        act2.sa_handler = stop_netfilter_ioctl;
        sigemptyset(&act2.sa_mask);
        act2.sa_flags = 0;

        if (sigaction(SIGUSR2, &act2, NULL) == -1)
            exit_sys("sigaction");

        printf("c switch specified, netfilterhook started...\n Press enter to stop...\n\n");
        fork_execute("../netfilter/load");
        sleep(1);
        

        if ((fd = open("/proc/proc-dev", O_RDONLY)) == -1)
            exit_sys("open");

        while (1)
            sleep(1);

        close(fd);
    }

    return 0;
}

void exit_sys(const char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void exit_fail(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}

void start_netfilter_ioctl()
{
    if (ioctl(fd, IOC_START_HOOK) == -1)
        exit_sys("ioctl");
}

void stop_netfilter_ioctl()
{
    if (ioctl(fd, IOC_STOP_HOOK) == -1)
        exit_sys("ioctl");
}
