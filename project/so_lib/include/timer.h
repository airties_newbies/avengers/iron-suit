#ifndef TIMER_H_
#define TIMER_H_



#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

timer_t gTimerid;
void timer_handler(int sig,siginfo_t *info, void * ucontext);
int start_timer(void);
void delete_timer(void);

#ifdef __cplusplus
}
#endif

#endif /* TIMER_H_ */

