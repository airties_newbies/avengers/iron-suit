#ifndef FORK_H_
#define FORK_H_


#ifdef __cplusplus
extern "C" {
#endif

#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>


int fork_execute( char *line);


#ifdef __cplusplus
}
#endif

#endif /* FORK_H_ */













