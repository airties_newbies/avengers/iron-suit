#include  "fork_and_execute.h"


int fork_execute( char *line)
{

	char  *argv[64];
	pid_t  pid;
	int    status;
	int i = 0;

	while (*line != '\0') {
		while (*line == ' ' || *line == '\t' || *line == '\n') {
			*line = '\0';
			line++;
			
		}
		argv[i] = line;
		i++;
		while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') {
			line++;
		}
	}
	argv[i] = '\0';

	
	/* fork & execute*/
	if ((pid = fork()) < 0) {
		printf("@@@@@@@@@@@@@@@@@@@@@ pid = for failed return -1 called\n");
		return -1;
	}
	else if (pid == 0) {
		printf("@@@@@@@@@@@@@@@@@@@@@ execv called\n");
		printf("%s %s\n", argv[0], argv[1]);
		if (execv(argv[0], argv) < 0) {
			
			printf("@@@@@@@@@@@@@@@@@@@@@ execv failed return -1 called \n");
			return -1;
		}
	}
	else {
		while (wait(&status) != pid);
		
		
	}

		printf("@@@@@@@@@@@@@@@@@@@@@ return 0 called\n");
	return 0;
}












