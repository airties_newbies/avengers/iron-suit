

//gcc -I. -c timer.c

#include "timer.h"


void timer_handler(int sig,siginfo_t *info, void * ucontext) {
	printf("Catched timer signal: %d ...  with value %d !!\n", sig,info->si_value.sival_int);
	
}

void delete_timer(void){

	timer_delete(gTimerid);

}

int start_timer(void)
{
	struct itimerspec its;
	struct sigaction sa;
	struct sigevent sev;

	sigemptyset(&sa.sa_mask);
	sa.sa_flags =SA_SIGINFO | SA_RESTART ;
	sa.sa_sigaction = timer_handler;
		
	/* Create the timer */
	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIGRTMIN;
	sev.sigev_value.sival_int = 5;
	
	its.it_value.tv_sec = 2;
	its.it_interval.tv_sec = 2;
	its.it_interval.tv_nsec = 0;
	its.it_value.tv_nsec = 0;

	if(-1 == timer_create(CLOCK_REALTIME, &sev, &gTimerid))
		return -1;

	if(-1 == sigaction(SIGRTMIN, &sa, NULL))
		return -2;

	if(-1 == timer_settime(gTimerid, 0, &its, NULL))
		return -3;

	return 0;
}

