#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define PIPE_MAGIC                  'a'
#define IOC_START_HOOK         _IOR(PIPE_MAGIC, 0, int) 
#define IOC_STOP_HOOK         _IOR(PIPE_MAGIC, 1, int) 


void exit_sys(const char *msg);
void exit_fail(const char *msg);

int main(int argc, char **argv)
{   
    int fd;
      
    if ((fd = open("/proc/proc-dev", O_RDONLY)) == -1)
        exit_sys("open");
    
    
    if (ioctl(fd, IOC_START_HOOK) == -1)
        exit_sys("ioctl");
    
    getchar();

    if (ioctl(fd, IOC_STOP_HOOK) == -1)
        exit_sys("ioctl");
    
    close(fd);
    
    return 0;    
}


void exit_sys(const char *msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

void exit_fail(const char *msg)
{
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}
