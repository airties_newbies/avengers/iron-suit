#include <linux/module.h>
#include <linux/skbuff.h>          
#include <linux/init.h>
#include <net/sock.h>
#include <linux/inet.h>
#include <linux/ip.h>             
#include <linux/kernel.h> 
#include <linux/netfilter.h>
#include <uapi/linux/netfilter_ipv4.h> 
#include <linux/proc_fs.h>
 
 #include "netfilter.h"

 
static struct nf_hook_ops netfops;                    
static long  generic_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
static void start_hook(void);
static void stop_hook(void);

static struct proc_ops g_proc_ops = {
    //.owner = THIS_MODULE,
 	.proc_ioctl=generic_ioctl,
};

static unsigned int main_hook(void *priv, struct sk_buff *skb, const struct nf_hook_state *state)

{
    struct iphdr *iph;
 
 	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);
 	
    iph = ip_hdr(skb);
    if(iph->daddr == in_aton("192.168.1.6"))
    { 
    	printk("%s[%d] NF_DROP\n", __FUNCTION__, __LINE__);
        return NF_DROP; 
    }
    
   	printk("%s[%d] NF_ACCEPT\n", __FUNCTION__, __LINE__);
    return NF_ACCEPT;
}



int __init nf_module_init(void)
{

    if (proc_create("proc-dev", S_IRUSR|S_IRGRP|S_IROTH, NULL, &g_proc_ops) == NULL) {
        printk(KERN_INFO "Can not create proc file!...\n");
        return -ENOMEM;
    }
    
 	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);
    return 0;
}




static long  generic_ioctl(struct file *filp, unsigned int cmd, unsigned long arg){

	 switch (cmd) {
        case IOC_START_HOOK:
             start_hook();
             break;            
        case IOC_STOP_HOOK:
            stop_hook();
            break;
    }
    
    return 0;   

}
 

static void start_hook(void){
   
    netfops.hook              =       main_hook;
    netfops.pf                =       PF_INET;        
    netfops.hooknum           =       0;
    netfops.priority          =       NF_IP_PRI_FIRST;
    
    nf_register_net_hook(&init_net, &netfops);
 
 	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);

}


static void stop_hook(void){
   
  	nf_unregister_net_hook(&init_net, &netfops);
  	
 	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);

}



 
void  nf_module_exit(void) 
{ 
	remove_proc_entry("proc-dev", NULL); 
	printk("%s[%d] entered\n", __FUNCTION__, __LINE__);
    //nf_unregister_hook(&netfops); 
}


module_init(nf_module_init);
module_exit(nf_module_exit);
MODULE_LICENSE("GPL");



