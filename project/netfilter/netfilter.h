#ifndef PIPE_DRIVER_H_
#define PIPE_DRIVER_H_


#define PIPE_MAGIC            'a'
#define IOC_START_HOOK        _IOR(PIPE_MAGIC, 0, int) 
#define IOC_STOP_HOOK         _IOR(PIPE_MAGIC, 1, int) 

#endif /* PIPE_DRIVER_H_ */