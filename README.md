# **Team Members**
- Seniha Çelik
- Nefi Güçlü
- Haktan Cengiz
- Onur Yıldız

# Part-1: Configuring git and ssh-key in computer

This part is referanced from [tutorial](https://www.attosol.com/manage-multiple-git-accounts/) send by Ertan Uysal.

> :warning: **WARNING:** *Part-1* is still under construction, but the following instructions should be enough for you to setup 2 different git profiles in your computer.

There is already one account in our computers set for our company. To seperate those accounts we have to configure several files:
- ~/.gitconfig
- ~/.ssh
- ~/.gitconfig-personal
- ~/personal-repos/

1. Generate another ssh-key for this account by `ssh-keygen -t rsa -b 4096 -C "your@gmail.com" -f "personal_id_rsa" `.

2. There will be 2 file in the directory in wihch you run this command. See them by `ls personal_id_rsa*`, we expected to see:
```sh
personal_id_rsa
personal_id_rsa.pub

```





3. Define the content of your [public key](a "personal_id_rsa.pub") to Gitlab.
    1. Open your Gitlab account.
    2. Click the Avatar at right-up, and click *Edit profile*.
    3. Click the *SSH Keys* tab that you will see on left-side.
    4.  Paste the [content of your public key](a "This is a basic text file, you can see its content by any text editor. If you want you can also use `cat ~/.ssh/personal_id_rsa.pub`") into *Key* part, and give it any title you want.
    5. Click *Add key* button, and save the key. We have succesfully added our ssh-key to Gitlab.

4. Move them inside *~/.ssh/* by `mv personal_id_rsa* ~/.ssh/`.

5. Now, except from your companies ssh-key the will be another ssh-key inside *~/.ssh/* directory. Therefore, we need to configure them.

6. Open *~/.ssh/config* by `gedit ~/.ssh/config`, and fill it with respect to link given above. ([Example one](https://airties-my.sharepoint.com/:u:/p/aziz_kavas/EVjurVKR2cBBszpdjRww7RIBC8qGl7uy-zfgwzEfJ3x-Ww?e=kKMlLb))


7. Create *~/personal-repos/* with `mkdir ~/personal-repos/`, and open *~/.gitconfig-personal* with `gedit ~/.gitconfig-personal` to configure it. ([Example one](https://airties-my.sharepoint.com/:u:/p/aziz_kavas/EZ27VL_BQT9JuwihY-gb-fsBg-RsoKAp_2AFVnVIepcopQ?e=ZrcODK))

8. Since our company's git config are complex for me. Instead of putting them into another directory I put my personel git into specific directory :joy: . Namely, put following lines into *.gitconfig* file.  `gedit ~/.gitconfig`

```sh
[includeIf "gitdir:~/personal-repos/"]
    path = ~/.gitconfig-personal
``` 
where *~/personal-repos/* will be your persenol repository, and *~/.gitconfig-personal* will be config file of it.

9. If your setup is correct, now you should be able to run following commands.
```sh
cd ~/personal-repos/
git clone git@gitlab.com:airties_newbies/initial-repo.git
```
---

# Part-2: Definition of the project 

---

# Notes from Ertan abi

## TODOs
- [ ] gitlab.com'da bir repo aç
- [ ] Verilen C örnek kod'larını birleştir
- [ ] netfilter hook yapan bir kernel module + Makefile
- [ ] bunu kullanan bir main içeren userspace bir app
- [ ] Bunun içine basit bir .so kullanan mekanizma ve Makefile
- [ ] getopt, sigaction, timer gibi örnek verilen kodları yan özellikleri de ekleyelim.
- [ ] Herşey'i head'e commitleyelim.
- [ ] gitlab'da issue tracking, derleme, otomatik test etme ortamları da var. Bunlarda aktif edilebilir.
- [ ] 12 kişilik iş değil bu. 4'lü gruplar halinde aynı ödev yapılsa daha mantıklı olur gibi geliyor. random gruplara karar verirsiniz olurmu sizce ?
 
